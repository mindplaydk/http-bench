<?php

header("Content-Type: image/jpeg");

while (ob_get_level() > 0) {
    ob_end_clean();
}

$source = fopen(__DIR__ . "/image.jpg", "rb");

$dest = fopen("php://output", "wb");

stream_copy_to_stream($source, $dest);
