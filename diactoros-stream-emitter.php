<?php

use Zend\Diactoros\Response\SapiStreamEmitter;
use Zend\Diactoros\Stream;

require __DIR__ . '/vendor/autoload.php';

$body = new Stream(__DIR__ . '/image.jpg');

$response = new Zend\Diactoros\Response($body);

$response = $response
    ->withHeader("Content-Type", "image/jpeg");

$emitter = new SapiStreamEmitter();

$emitter->emit($response);
