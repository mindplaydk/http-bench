<?php

use Zend\Diactoros\Response\SapiEmitter;
use Zend\Diactoros\Stream;

require __DIR__ . '/vendor/autoload.php';

$body = new Stream(__DIR__ . '/image.jpg');

$response = new Zend\Diactoros\Response($body);

$response = $response
    ->withHeader("Content-Type", "image/jpeg");

$emitter = new SapiEmitter();

$emitter->emit($response);
