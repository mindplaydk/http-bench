<?php

namespace http_bench;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response;
use Zend\Diactoros\Stream;

class Bootstrap
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next = null)
    {
        $body = new Stream(dirname(__DIR__) . '/image.jpg');

        $response = new Response($body);

        $response = $response->withHeader("Content-Type", "image/jpeg");

        return $response;
    }
}
